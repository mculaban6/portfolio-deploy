import "./index.scss";
import { useState, useEffect } from "react";
import LogoS from "../../assets/images/mark-logo.png";
import LogoSubtitle from "../../assets/images/mark_sub.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faLinkedin,
  faGithub,
  faGoogle,
  faSkype,
  faFacebook,
} from "@fortawesome/free-brands-svg-icons";
import {
  faHome,
  faUser,
  faPen,
  faEnvelope,
  faPhone,
  faSuitcase,
  faBars,
  faClose,
  faCode,
} from "@fortawesome/free-solid-svg-icons";
import { Link, NavLink, useLocation } from "react-router-dom";
import { Button } from "react-bootstrap";
export default function Sidebar() {
  const [showNav, setShowNav] = useState(false);
  const hamburgerIcon = document.querySelector(".hamburger-icon");
  const navBar = document.querySelector(".nav-bar");

  const handleToggleNav = () => {
    setShowNav(!showNav);
  };

  return (
    <>
      <div className="nav-bar">
        <Link className="logo" to="/" onClick={() => setShowNav(false)}>
          <img src={LogoS} alt="logo" />
          {/* <img className="sub-logo" src={LogoSubtitle} alt="mark" /> */}
        </Link>
        <nav className={showNav ? "mobile-show" : ""}>
          <NavLink
            // exact={true}
            activeClassName="active"
            className="home-link"
            to="/"
            onClick={() => setShowNav(false)}
          >
            <FontAwesomeIcon icon={faHome} color="#808080" />
          </NavLink>

          <NavLink
            activeClassName="active"
            className="about-link"
            to="/about"
            onClick={() => setShowNav(false)}
          >
            <FontAwesomeIcon icon={faUser} color="#4d4d4e" />
          </NavLink>

          <NavLink
            activeClassName="active"
            className="skill-link"
            to="/skills"
            onClick={() => setShowNav(false)}
          >
            <FontAwesomeIcon icon={faPen} color="#4d4d4e" />
          </NavLink>

          <NavLink
            activeClassName="active"
            className="project-link"
            to="/projects"
            onClick={() => setShowNav(false)}
          >
            <FontAwesomeIcon icon={faCode} color="#4d4d4e" />
          </NavLink>

          <NavLink
            activeClassName="active"
            className="contact-link"
            to="/contact"
            onClick={() => setShowNav(false)}
          >
            <FontAwesomeIcon icon={faPhone} color="#4d4d4e" />
          </NavLink>
        </nav>
        <ul>
          <li>
            <a
              href="https://www.linkedin.com/in/markculaban/"
              target="_blank"
              rel="noreferrer"
            >
              <FontAwesomeIcon
                icon={faLinkedin}
                color="#4d4d4e"
                className="anchor-icon"
              />
            </a>
          </li>
          <li>
            <a
              href="mailto:culaban.markarnold@gmail.com"
              target="_blank"
              rel="noreferrer"
            >
              <FontAwesomeIcon
                icon={faGoogle}
                color="#4d4d4e"
                className="anchor-icon"
              />
            </a>
          </li>
        </ul>

        <FontAwesomeIcon
          icon={showNav ? faClose : faBars}
          color="#ed6a40"
          size="3x"
          className="hamburger-icon"
          onClick={handleToggleNav}
        />
      </div>
      ;
    </>
  );
}

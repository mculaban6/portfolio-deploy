// import Banner from "../Banner";
// import Highlights from "../Highlights";

// const Projects = () => {
//   return (
//     <>
//       rgb(223, 83, 28)"projectContainer">
//         <Banner />
//         <Highlights />
//       </div>
//     </>
//   );
// };

// export default Projects;

import React, { useState, useEffect } from "react";
import "./index.scss";

function Projects() {
  const [offsetY, setOffsetY] = useState(0);
  const handleScroll = () => setOffsetY(window.pageYOffset);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  return (
    <div className="projectBody">
      <div className="projectContainer">
        <div id="projectTitle" class="slide projectHeader">
          <h1 className="titleHeader">PROJECTS</h1>
        </div>

        <div id="slide1" class="slide">
          <div class="projectTitle">
            <h1 className="projectHeader">E-Commerce API</h1>
            <p className="projectPara">
              An E-commerce API built using the MongoDB, Express.js, React, and
              Node.js (MERN) stack, and tested using Postman.
            </p>
          </div>
        </div>

        <div id="slide2" className="slide">
          <div className="projectTitle" id="projectSlide2">
            <h1 className="projectHeader">E-Commerce Site</h1>
            <p className="projectPara projectPara2">
              An e-commerce website built using the React JavaScript library for
              the front-end and with a backend for managing data and serving API
              endpoints.
            </p>
          </div>

          <div className="projectImage">
            <div class="carousel-wrapper">
              <div class="carousel-container">
                <div class="carousel">
                  <div class="image-one imageProject"></div>
                  <div class="image-two  imageProject"></div>
                  <div class="image-three  imageProject"></div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="slide3" class="slide">
          <div class="projectTitle">
            <h1 className="projectHeader">Under Development</h1>
            <p className="projectPara">
              <ul>
                <li> Bookeeping System</li>
                <li> Expenses Tracker</li>
                <li>Basic Engineering Calculator</li>
              </ul>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Projects;

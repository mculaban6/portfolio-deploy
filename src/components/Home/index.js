import "./index.scss";

import Logo from "../Logo";
import CustomLoader from "../Loader";
import { useState, useEffect } from "react";

export default function Home() {
  const [isLoading, setIsLoading] = useState(true);
  const [letterClass, setLetterClass] = useState("text-animate");

  useEffect(() => {
    const timer = setTimeout(() => {
      setLetterClass("text-animate-hover");
      setIsLoading(false);
    }, 2000);

    return () => clearTimeout(timer);
  }, []);

  if (isLoading) {
    return <CustomLoader />;
  }

  return (
    <div className="home-page-container">
      <div className="container home-page">
        <div className={`text-zone ${letterClass}`}>
          {/* <img src={NameTitle} alt="developer" /> */}
        </div>
        <Logo />
      </div>
    </div>
  );
}

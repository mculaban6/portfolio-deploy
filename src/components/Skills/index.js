import React, { useEffect, useState } from "react";
import "./index.scss";
import CustomLoader from "../Loader";
import Sphere from "../Sphere";

export default function Skills() {
  const [isLoading, setIsLoading] = useState(true);
  // Simulate a loading delay

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsLoading(false);
    }, 3000);

    return () => clearTimeout(timer);
  }, []);

  if (isLoading) {
    return <CustomLoader />;
  }

  return (
    <>
      <div className="container about-page skills-page">
        <div className="text-zone">
          <h1 className="h1Skills">
            <div class="signSkill">
              <span class="fast-flickerSkills">S</span>
              <span className="glowClass">ki</span>
              <span class="flickerSkills">ll</span>
              <span className="glowClass">s</span>
            </div>
          </h1>
          <p align="LEFT" className="skillsPara">
            I'm a skilled web developer with expertise in HTML, CSS, SCSS,
            ReactJS, React Bootstrap, NodeJS, and MongoDB. I'm experienced in
            Git and Postman and can create intuitive, visually appealing web
            applications with robust back-end systems. I'm confident in
            delivering high-quality results that meet the needs of clients and
            end-users.
          </p>
        </div>
        <h2>
          <Sphere />
        </h2>
      </div>
    </>
  );
}

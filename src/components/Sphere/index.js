import React, { useEffect, useState } from "react";
import "./index.scss";
import CustomLoader from "../Loader";
import TagCloud from "TagCloud"; // Import the library

const Sphere = () => {
  useEffect(() => {
    const container = ".tagcloud";
    const texts = [
      "HTML",
      "CSS",
      "SASS",
      "JavaScript",
      "React",
      "Vue",
      "Nuxt",
      "NodeJS",
      "Babel",
      "Jquery",
      "ES6",
      "GIT",
      "GITHUB",
      "3d",
      "CAD",
      "OOP",
    ];

    const options = {
      radius: 400,
      maxSpeed: "normal",
      initSpeed: "normal",
      keep: true,
    };
    TagCloud(container, texts, options);
  }, []);

  return (
    <>
      <div className="text-shpere">
        <span className="tagcloud"></span>
      </div>
    </>
  );
};

export default Sphere;

import "./index.scss";
import LogoTitle from "../../assets/images/mark-logo.png";
import NameTitle from "../../assets/images/MARKCULABAN.png";
import { useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";

export default function Logo() {
  const bgRef = useRef();
  const outlineLogoRef = useRef();
  const solidLogoRef = useRef();
  const navigate = useNavigate();

  const handleContactClick = () => {
    navigate("/contact");
  };

  return (
    <div className="logo-container" ref={bgRef}>
      <div className="imageLogo">
        <img
          className="solid-logoTitle"
          ref={solidLogoRef}
          src={LogoTitle}
          alt="JavaScript,  Developer"
        />

        <img
          className="solid-logoName"
          ref={solidLogoRef}
          src={NameTitle}
          alt="JavaScript,  Developer"
        />
        <div className="contactButton">
          <button className="buttonHome" onClick={handleContactClick}>
            CONTACT ME
          </button>
        </div>
      </div>
    </div>
  );
}

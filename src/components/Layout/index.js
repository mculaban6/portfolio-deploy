import { Outlet } from "react-router-dom";
import Sidebar from "../Sidebar/";
import "./index.scss";
import { useState, useEffect } from "react";
import CustomLoader from "../Loader";

const Layout = () => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsLoading(false);
    }, 3000);

    return () => clearTimeout(timer);
  }, []);

  if (isLoading) {
    return <CustomLoader />;
  }

  return (
    <div className="App">
      <Sidebar />

      <div className="page">
        {/* <span className="tags top-tags">&lt;body&gt;</span> */}
        <Outlet />
        <span className="tags bottom-tags"></span>
      </div>
      <div class="area">
        <ul class="circles">
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
    </div>
  );
};

export default Layout;

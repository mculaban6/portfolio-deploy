import { useEffect, useState } from "react";
import {
  faAngular,
  faCss3,
  faGitAlt,
  faHtml5,
  faJsSquare,
  faReact,
  faBootstrap,
} from "@fortawesome/free-brands-svg-icons";

import AnimatedLetters from "../AnimatedLetters";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./index.scss";
import CustomLoader from "../Loader";

export default function About() {
  const [letterClass, setLetterClass] = useState("text-animate");
  const [isLoading, setIsLoading] = useState(true);
  // Simulate a loading delay

  useEffect(() => {
    const timer = setTimeout(() => {
      setLetterClass("text-animate-hover");
      setIsLoading(false);
    }, 2000);

    return () => clearTimeout(timer);
  }, []);

  if (isLoading) {
    return <CustomLoader />;
  }

  return (
    <>
      <div className="container about-page">
        <div className="text-zone">
          <h1>
            <AnimatedLetters
              letterClass={letterClass}
              strArray={[
                "H",
                "i",
                "!",
                "",
                "",
                "I",
                "'",
                "m",
                "",
                <section class="animation">
                  <div class="first">
                    <div>Mark Arnold Culaban</div>
                  </div>
                  <div class="second">
                    <div>Full Stack Developer</div>
                  </div>
                  <div class="third">
                    <div>Mechanical Engineer</div>
                  </div>
                </section>,
              ]}
              idx={15}
            />
          </h1>
          <div className="paraAboutContainer">
            <p className="paraAbout" align="LEFT">
              As a coding bootcamp graduate with a background in mechanical
              engineering, I bring a unique perspective to web development. My
              diverse skillset allows me to approach problems creatively and
              efficiently, and I am passionate about creating impactful digital
              products that solve real-world problems. With experience in both
              engineering and web development, I have developed a strong
              analytical mindset and a keen eye for detail. I am constantly
              seeking new challenges and opportunities to learn and improve my
              skills, and am excited to apply them to help create innovative
              digital products with a real-world impact.
            </p>
          </div>
        </div>
        <div className="stage-cube-cont">
          <div className="cubespinner">
            <div className="face">
              <FontAwesomeIcon icon={faHtml5} color="#F06529" />
            </div>
            <div className="face">
              <FontAwesomeIcon icon={faCss3} color="#28A4D9" />
            </div>
            <div className="face">
              <FontAwesomeIcon icon={faReact} color="#5ED4F4" />
            </div>
            <div className="face">
              <FontAwesomeIcon icon={faJsSquare} color="#EFD81D" />
            </div>
            <div className="face">
              <FontAwesomeIcon icon={faGitAlt} color="#EC4D28" />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

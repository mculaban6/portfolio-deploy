import React from "react";
import "./index.scss";

const CustomLoader = () => {
  return (
    <div>
      <span class="loader"></span>;
    </div>
  );
};

export default CustomLoader;
